# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 20:20 
# @name: select_teac
# @author：Fei Dexu
import common
import pymysql

# 建立连接
conn = common.get_conn(database='school', cursorclass=pymysql.cursors.DictCursor)

try:
    with conn.cursor() as cursor:
        result = cursor.execute('select * from tb_teacher')
        print(cursor.fetchone())
        print(*cursor.fetchmany(5))
        print(*cursor.fetchall())


except:
    print('查询失败！')

finally:
    conn.close()

