# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 20:06 
# @name: update_teac
# @author：Fei Dexu

import common
import pymysql

teac_id = int(input('请输入需要修改的教师编号：'))
teac_name = input('请输入教师姓名：')
teac_title = input('请输入教师职称：')
conn = common.get_conn(database='school', cursorclass=pymysql.cursors.DictCursor)

try:
    with conn.cursor() as cursor:
        result = cursor.execute('update tb_teacher set teacname=%s, teactitle=%s where teacid=%s',
                                (teac_name, teac_title, teac_id))

    if result == 1:
        print('修改成功')
        conn.commit()

except pymysql.MySQLError as err:
    print(err, '修改失败！！')
    conn.rollback()
finally:
    conn.close()



