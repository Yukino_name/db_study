# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 17:37 
# @name: create_teac
# @author：Fei Dexu
import common
import pymysql

teac_id = int(input('请输入教师编号：'))
teac_name = input('请输入教师姓名：')
teac_title = input('请输入教师职称：')
col_id = int(input('请输入所属学院：'))
conn = common.get_conn(database='school', cursorclass=pymysql.cursors.DictCursor)

try:
    with conn.cursor() as cursor:
        result = cursor.execute('insert into tb_teacher values (%s, %s, %s, %s)',
                                (teac_id, teac_name, teac_title, col_id))

    if result == 1:
        print('添加教师成功')
        conn.commit()

except pymysql.MySQLError as err:
    print(err, '添加失败')
    conn.rollback()
finally:
    conn.close()











