# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 19:58 
# @name: deleate_teac
# @author：Fei Dexu
import common
import pymysql

teac_id = int(input('请输入教师编号：'))
conn = common.get_conn(database='school', cursorclass=pymysql.cursors.DictCursor)

try:
    with conn.cursor() as cursor:
        result = cursor.execute('delete from tb_teacher where teacid=%s', teac_id)

    if result == 1:
        print('删除成功成功')
        conn.commit()  # 手动提交

except pymysql.MySQLError as err:
    print(err, '删除失败！！')
    conn.rollback()
finally:
    conn.close()  # 释放连接
