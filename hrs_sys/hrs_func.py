# -*- coding: UTF-8 -*-
# @date: 2019/10/30 0030 19:04 
# @name: hrs_func
# @author：Fei Dexu

import pymysql
# from functools import partial

# 这里会存放一些所需要的函数

conn = pymysql.connect(host='114.55.28.188', port=3306, user='root', password='123456',
                       database='hrs', charset='utf8', autocommit=True,
                       cursorclass=pymysql.cursors.DictCursor)


def index():
    # print('请输入你想选择的操作：')
    print('='*30)
    print('1.添加数据')
    print('2.查询数据')
    print('3.修改数据')
    print('4.删除数据')
    print('5.退出系统')
    print('=' * 30)


# 添加页面设计
def add_data():
    print('=' * 30)
    print('1.添加部门')
    print('2.添加人员')
    print('3.返回上一层')
    print('4.退出系统')
    print('=' * 30)


# 查询页面设计
def sel_data():
    print('=' * 30)
    print('1.查询部门')
    print('2.查询人员')
    print('3.返回上一层')
    print('4.退出系统')
    print('=' * 30)


# 修改页面设计
def upd_data():
    print('=' *  30)
    print('1.修改部门')
    print('2.修改人员')
    print('3.返回上一层')
    print('4.退出系统')
    print('=' * 30)


# 删除页面设计
def del_data():
    print('=' * 30)
    print('1.删除部门')
    print('2.删除人员')
    print('3.返回上一层')
    print('4.退出系统')
    print('=' * 30)


# ==========================部门=====================
# 添加部门函数
def add_dept():
    dno = int(input('请输入部门编号：'))
    dname = input('请输入部门名字：')
    dlocal = input('请输入部门地点：')
    try:
        with conn.cursor() as cursor:
            result = cursor.execute('insert into tb_dept values (%s, %s, %s)', (dno, dname, dlocal))
            if result == 1:
                print('添加成功！')

    except pymysql.MySQLError as err:
        print(err, '添加失败！！')
        conn.rollback()

    finally:
        conn.close()


# 2.查询部门
def sel_dept():
    print('=' * 30)
    print('1.按部门编号查询')
    print('2.按部门名字查询')
    print('其他：退出查询')
    print('=' * 30)
    num = int(input('请输入你的选择：'))
    if num == 1:
        d_no = int(input('请输入要查询的部门编号：'))
        try:
            with conn.cursor() as cursor:
                cursor.execute('select * from tb_dept where dno=%d', d_no)
                print(cursor.fetchall())

        except pymysql.MySQLError as err:
            print(err, '查询失败,没有此部门！!')
            conn.rollback()

        finally:
            conn.close()
    elif num == 2:
        d_name = input('请输入要查询的部门名字：')
        try:
            with conn.cursor() as cursor:
                cursor.execute('select * from tb_dept where dname="%s"', d_name)
                print(cursor.fetchall())

        except pymysql.MySQLError as err:
            print(err, '查询失败,没有此部门！!')
            conn.rollback()

        finally:
            conn.close()
    else:
        return index()



# ==========================部门=====================


def add_person():
    index()





