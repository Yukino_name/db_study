# -*- coding: UTF-8 -*-
# @date: 2019/10/31 0031 17:16 
# @name: conn-redis
# @author：Fei Dexu

import json
import random
import redis
import requests
import pymysql

def num(length=6):
    # 生成随机验证码
    return ''.join(random.choices('0123456789', k=length))  # k命名关键字参数


# for _ in range(10):
#     print(num())


def send_message_example(tel, code):
    resp = requests.post(
        url='http://sms-api.luosimao.com/v1/send.json',
        auth=('api', 'key-c6305c397d86ab1093b46ad228e5a3fb'),
        data={
            'mobile': tel,
            'message': f'您的短信验证码是{code}，打死也不能告诉别人哟！【Python小课】'
        },
        timeout=5,
        verify=False
    )
    return json.loads(resp.text)


# print(send_message_example('13548041193', '123456'))

tel = input('请输入手机号：')
code = num()
# send_message_example(tel, code)

conn = redis.Redis(host='114.55.28.188', port=6379, password='123456', charset='utf8')
sqlconn = pymysql.connect(host='114.55.28.188', port=3306, user='root', password='123456',
                          database='school', charset='utf8')  # , autocommit=True ---》自动提交


if conn.get(f'block:{tel}'):
    print('请在120秒内不要重复发送验证码！')
    conn.incr(f'block:{tel}')
else:
    result = send_message_example(tel, code)
    if result['error'] == 0:
        print('短信验证码发送成功！！')
        conn.set(f'valid:{tel}', code, ex=600)
        conn.set(tel, code, ex=120)






