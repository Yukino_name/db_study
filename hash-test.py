# -*- coding: UTF-8 -*-
# @date: 2019/10/31 0031 15:33 
# @name: hash-test
# @author：Fei Dexu

import hashlib

# 经典的哈希算法--》MD5/ SHAI/ SHA256 / SHA512
# 单向哈希函数---》数字签名/指纹 --》防伪造防止篡改
hash1 = hashlib.md5()
with open('Python-3.8.0.tar.xz', 'rb') as file:
    for data in iter(lambda: file.read(1024), b''):
        hash1.update(data)
    # data = file.read(1024)
    # while data:
    #     hash1.update(data)
    #     data = file.read(1024)

print(hash1.hexdigest())


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


        

item2 = {
    [1, 2, 3]: 'hello'
}

print(item2)


