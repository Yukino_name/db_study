# -*- coding: UTF-8 -*-
# @date: 2019/11/1 0001 12:25 
# @name: sql-redis
# @author：Fei Dexu

# import json
import common
import pickle
import redis
import pymysql
import time


def addTime(func):

    def wrapper(*args, **kwargs):
        starttime = time .time()
        result = func(*args, **kwargs)
        endtime = time.time()
        print(f'{func.__name__}执行时间：{endtime - starttime}秒')

        return result
    return wrapper


@addTime  # 相当于addTime(selPeo)
def selPeo():
    # 获取部门数据
    # start = time.time()
    conn = redis.Redis(host='114.55.28.188', port=6379, password='123456', charset='utf8')
    catch_data = conn.get('depts')
    depts = pickle.loads(catch_data) if catch_data else None
    if not depts:
        sqlconn = common.get_conn(database='hrs')
        try:
            with sqlconn.cursor(pymysql.cursors.DictCursor) as cursor:
                cursor.execute('select * from tb_dept')
                depts = cursor.fetchall()
            conn.set('depts', pickle.dumps(depts), ex=3600)
        except pymysql.MySQLError as err:
            print('查询失败！')
        finally:
            conn.close()
    # decr_time = time.time() - start
    # print(decr_time)
    return depts


results = selPeo()
for re in results:
    print(re)





















