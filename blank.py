# -*- coding: UTF-8 -*-
# @date: 2019/10/31 0031 9:49 
# @name: blank
# @author：Fei Dexu

from time import sleep

from threading import Thread, Lock


class Account(object):

    def __init__(self):
        super().__init__()
        self.balance = 0
        self.lock = Lock()

    def deposit(self, money):
        with self.lock:   # with自动执行加锁与解锁
            new_balance = self.balance + money
            sleep(0.1)
            self.balance = new_balance

        # self.lock.acquire()  # 加锁
        # try:
        #     new_balance = self.balance + money
        #     sleep(0.1)
        #     self.balance = new_balance
        # finally:
        #     self.lock.release()  # 释放锁


account = Account()

threads = []
for _ in range(100):
    t = Thread(target=account.deposit, args=(1, ))
    threads.append(t)
    t.start()

for t in threads:
    t.join()

print(account.balance)



# class createThread(Thread):
#     def __init__(self):
#         super().__init__()






















