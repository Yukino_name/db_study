# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 14:30 
# @name: example03
# @author：Fei Dexu
import pymysql

stu_id = int(input('请输入你的学号：'))

stu_name = input('请输入学生姓名：')
stu_sex = int(input('请输入学生性别：'))
stu_birth = input('请输入学生生日：')
stu_addr = input('请输入学生地址：')
colid = input('请输入学院编号：')

# 1.建立连接
conn = pymysql.connect(host='114.55.28.188', port=3306, user='root', password='123456',
                       database='school', charset='utf8')

try:
    # 获取游标
    with conn.cursor() as cursor:
        # 执行SQL添加语句
        result = cursor.execute('update tb_student set stuname=%s,stusex=%s,stubirth=%s,stuaddr=%s,colid=%s where stuid=%s', (stu_name, stu_sex, stu_birth, stu_addr, colid, stu_id))
    if result == 1:
        print('修改成功！')
    # 成功提交
    conn.commit()
except:
    print('修改失败！！')
    # 失败回滚
    conn.rollback()

finally:
    conn.close()  # 关闭数据库的连接
