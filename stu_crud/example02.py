# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 14:15 
# @name: example02
# @author：Fei Dexu

import pymysql

stu_id = int(input('请输入需要删除的学生学号：'))

# 1.建立连接
conn = pymysql.connect(host='114.55.28.188', port=3306, user='root', password='123456',
                       database='school', charset='utf8')  # , autocommit=True ---》自动提交

# 删除数据
try:
    # 获取游标
    with conn.cursor() as cursor:
        result = cursor.execute('delete from tb_student where stuid=%s', stu_id)

    if result == 1:
        print('删除学生成功！')
    conn.commit()
except:
    print('删除学生失败')
    conn.rollback()  # 添加失败回滚（重新添加）

finally:
    conn.close()  # 关闭数据库的连接















