# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 11:30 
# @name: example01
# @author：Fei Dexu

import pymysql

stu_id = int(input('请输入学生学号：'))
stu_name = input('请输入学生姓名：')
stu_sex = int(input('请输入学生性别：'))
stu_birth = input('请输入学生生日：')
stu_addr = input('请输入学生地址：')
colid = input('请输入学院编号：')

# 1.建立连接
conn = pymysql.connect(host='114.55.28.188', port=3306, user='root', password='123456',
                       database='school', charset='utf8')  # , autocommit=True ---》自动提交

try:
    # 2.获取游标
    # 建立一个游标对象---with(上下文语法，给游标设置作用域)
    with conn.cursor() as cursor:
        # 执行SQL添加语句
        result = cursor.execute('insert into tb_student values (%s, "%s", %s, "%s", "%s", %s)',
                                (stu_id, stu_name, stu_sex, stu_birth, stu_addr, colid))
    if result == 1:
        print('添加学生成功！')
    # 4.成功提交
    conn.commit()  # --->手动提交
except pymysql.MySQLError as err:
    print('err', '添加学生失败！！')
    # 失败回滚
    conn.rollback()  # 添加失败回滚（重新添加）

finally:
    # 5.释放连接
    conn.close()  # 关闭数据库的连接









