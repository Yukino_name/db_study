# -*- coding: UTF-8 -*-
# @date: 2019/10/29 0029 15:26 
# @name: example04
# @author：Fei Dexu

import pymysql
import common

# 查询
conn = common.get_conn(database='school', cursorclass=pymysql.cursors.DictCursor)
try:
    with conn.cursor() as cursor:
        cursor.execute('select * from tb_student')
        # print(cursor.fetchmany(5))
        # student = cursor.fetchone()
        # print(student['stuname'], student['stubirth'])
        # print(cursor.fetchmany(2))
        print(cursor.fetchone())

except pymysql.MySQLError as err:
    print(err, '抓取失败!!')

finally:
    conn.close()

